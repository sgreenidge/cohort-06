
if test -n "$1"; then
  for filename in "$@"; do
    #Arg1 has something
    if test ! -e $filename; then
        #File does not exists
        echo "File does not exist: $filename"
    elif test -s $filename; then
    	# File exists and is not empty
	echo "Inspected by Stephan" >> $filename
    else
    	# File exists and IS empty
	echo "Removing $filename"
	rm $filename	
    fi
  done
else
    #Arg2 has nothing
    echo "Please provide at least one filename."
fi

